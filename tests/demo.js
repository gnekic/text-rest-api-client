const textRestClient = require('./../src/index');

const main = async () => {

    const { sendSMS } = textRestClient({
        API_KEY: 'INSERT_YOUR_KEY',
    });

    const phoneNumber = 'XXX';
    const smsText = 'Test from api-client!';
    
    const sendingSMSResult = await sendSMS(phoneNumber, smsText);
    console.log(sendingSMSResult);

};
main();