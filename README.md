# Text.rest Node.js API client

This is simple client for text.rest api, written in Node.js.

Installation:

```
npm i https://bitbucket.org/gnekic/text-rest-api-client
```

Usage:

```
const textRestClient = require('text-rest-api-client');

const main = async () => {

    const { sendSMS } = textRestClient({
        API_KEY: 'INSERT_YOUR_KEY',
    });
    
    const sendingSMSResult = await sendSMS('XXXXXXXXX', 'Text.rest api-client test sms!');
    console.log(sendingSMSResult);

};
main();
```