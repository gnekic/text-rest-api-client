const request = require('request-promise-native');
const _ = require('lodash');

module.exports = (settings) => {
  
  const sendSMS = async (phone, text) => {
    const { 
      API_KEY,
      API_ENDPOINT,
      verbose
    } = settings;

    if (verbose === true) {
      console.log(API_KEY, API_ENDPOINT, verbose);
      console.log(phone, text);
    }

    // Starting the machine!
    const TARGET_URL = `${API_ENDPOINT}/sendSMS?key=${encodeURIComponent(API_KEY)}&phone=${encodeURIComponent(phone)}&text=${encodeURIComponent(text)}`;
  
    const headers = {
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8',
    };

    const post_data = {
      "key": API_KEY,
      "phone": phone,
      "text": text,
    };
    
    const options = {
      resolveWithFullResponse: true, // To show headers 
      simple: false, // To not fail on different HTTP code other than 200
      method: 'GET',
      url: TARGET_URL,
      json: post_data,
      headers,
    };
  
    return request(options)
      .then((response) => {
        return _.get(response, 'body');
      })
      .catch((error) => {
        if (verbose === true) {
          console.log(`######### CLIENT API ERROR #########`);
          console.log(JSON.stringify(error, null, 2));
          console.log(`######### CLIENT API ERROR #########`);
        }
        return undefined;
      });
  }

  return sendSMS;
};
