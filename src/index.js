const sendSMS = require('./api/sendSMS');

// Load all api calls and send settings after validation
module.exports = (userSettings) => {
    const defaultSettings = {
        API_ENDPOINT: 'https://text.rest',
        verbose: false,
    };
    // Valdate & override default settings
    const settings = {
        ...defaultSettings,
        ...userSettings,
    }
    // Pass settings and return functions
    return {
        sendSMS: sendSMS(settings),
    };
};
